import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }

  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });

  register(client: any) {
    return this.http.post(`${environment.baseurl}/register`, client);
  }

  getalluser() {
    return this.http.get(`${environment.baseurl}/getalluser`);
  }

  getbyid(id: any) {
    return this.http.get(`${environment.baseurl}/userbyid/${id}`);
  }

  confirmuser(id: any, newuser: any) {
    return this.http.put(`${environment.baseurl}/confirmeduser/${id}`, newuser);
  }

  deleteuser(id: any) {
    return this.http.delete(`${environment.baseurl}/deleteuser/${id}`);
  }
}
