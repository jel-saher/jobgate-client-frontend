import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlaceService {
  constructor(private http: HttpClient) {}
  getallplaces() {
    return this.http.get(`${environment.baseurl}/place/listplace`);
  }

  getoneplace(id: any) {
    return this.http.get(`${environment.baseurl}/place/byidplace/${id}`);
  }
}
