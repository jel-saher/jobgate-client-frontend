import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }
  getallMsg(){
    return this.http.get(`${environment.baseurl}/message//getmessage`);
  }
  getmessage(id:any){
return this.http.get(`${environment.baseurl}/message/getmessageById/${id}`);
  }
  deletemessage(id:any){
    return this.http.delete(`${environment.baseurl}/message/deletemessage/${id}`)
  }
  addmessage(message:any){
    return this.http.post(`${environment.baseurl}/message/createmessage`,message)
  }
}
