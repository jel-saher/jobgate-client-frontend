import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpecialityService {

  constructor(private http:HttpClient) { }

  getallspeciality() {
    return this.http.get(`${environment.baseurl}/speciality/listspeciality`);
  }

  getbyidspeciality(id: any) {
    return this.http.get(
      `${environment.baseurl}/speciality/byidspeciality/${id}`
    );
  }
}
