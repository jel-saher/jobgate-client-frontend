import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OffreService {
  constructor(private http: HttpClient) {}

  getalloffre() {
    return this.http.get(`${environment.baseurl}/offre/listoffre`);
  }
  getoneoffre(id: any) {
    return this.http.get(`${environment.baseurl}/offre/byidoffre/${id}`);
  }
  deleteoffre(id: any) {
    return this.http.delete(`${environment.baseurl}/offre/deleteoffre/${id}`);
  }

  addoffre(newoffre: any) {
    return this.http.post(`${environment.baseurl}/offre/createoffre`, newoffre);
  }
}
