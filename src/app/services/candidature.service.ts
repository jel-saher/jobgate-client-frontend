import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CandidatureService {
  constructor(private http: HttpClient) {}

  addcandidature(newcandidature: any) {
    return this.http.post(
      `${environment.baseurl}/candidature/createcandidature`,
      newcandidature
    );
  }

  getallcandidature() {
    return this.http.get(
      `${environment.baseurl}/candidature/getallcandidature`
    );
  }

  getcandidature(id: any) {
    return this.http.get(
      `${environment.baseurl}/candidature/getcandidaturebyid/${id}`
    );
  }
}
