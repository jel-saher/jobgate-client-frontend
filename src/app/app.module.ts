import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LayoutComponent } from './components/layout/layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ListoffreComponent } from './components/listoffre/listoffre.component';
import { DetailoffreComponent } from './components/detailoffre/detailoffre.component';
import { ListcandidatComponent } from './components/listcandidat/listcandidat.component';
import { DetailcandidatComponent } from './components/detailcandidat/detailcandidat.component';
import { ListentrepriseComponent } from './components/listentreprise/listentreprise.component';
import { AddoffreComponent } from './components/addoffre/addoffre.component';
import { MyoffreComponent } from './components/myoffre/myoffre.component';
import { CandidatureComponent } from './components/candidature/candidature.component';
import { CvComponent } from './components/cv/cv.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { DetailentrepriseComponent } from './components/detailentreprise/detailentreprise.component';
import { MycandidatureComponent } from './components/mycandidature/mycandidature.component';
import { MessageComponent } from './components/message/message.component';
import { DetailmessageComponent } from './components/detailmessage/detailmessage.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LayoutComponent,
    SidebarComponent,
    HomeComponent,
    ListoffreComponent,
    DetailoffreComponent,
    ListcandidatComponent,
    DetailcandidatComponent,
    ListentrepriseComponent,
    AddoffreComponent,
    MyoffreComponent,
    CandidatureComponent,
    CvComponent,
    DetailentrepriseComponent,
    MycandidatureComponent,
    MessageComponent,
    DetailmessageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PdfViewerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
