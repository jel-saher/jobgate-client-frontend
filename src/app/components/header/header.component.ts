import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor() {}

  ngOnInit(): void {
    
  }
  isconnect() {
    return localStorage.getItem('state') == '0' ? true : false;
  }

  isentreprise() {
    return this.userconnect.role == 'entreprise' ? true : false;
  }

  iscandidat() {
    return this.userconnect.role == 'candidat' ? true : false;
  }

  logout() {
    localStorage.clear();
  }
}
