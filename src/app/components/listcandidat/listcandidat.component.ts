import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-listcandidat',
  templateUrl: './listcandidat.component.html',
  styleUrls: ['./listcandidat.component.css'],
})
export class ListcandidatComponent implements OnInit {
  listuser: any;
  nbrcandidat: any;
  constructor(private registerservice: RegisterService) {}

  ngOnInit(): void {
    this.getalluser();
  }

  getalluser() {
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data'].filter(
        (element: any) => element.role == 'candidat'
      );
    });
  }

  
}
