import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MycandidatureComponent } from './mycandidature.component';

describe('MycandidatureComponent', () => {
  let component: MycandidatureComponent;
  let fixture: ComponentFixture<MycandidatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MycandidatureComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MycandidatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
