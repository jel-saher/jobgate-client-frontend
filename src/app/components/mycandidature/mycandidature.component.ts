import { Component, OnInit } from '@angular/core';
import { CandidatureService } from 'src/app/services/candidature.service';

@Component({
  selector: 'app-mycandidature',
  templateUrl: './mycandidature.component.html',
  styleUrls: ['./mycandidature.component.css'],
})
export class MycandidatureComponent implements OnInit {
  listoffre: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(private candidatureservice: CandidatureService) {}

  ngOnInit(): void {
    this.getcandidature();
  }

  getcandidature() {
    this.candidatureservice.getallcandidature().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.candidat._id == this.userconnect._id
      );
      console.log('liste my offre :', this.listoffre);
    });
  }
}
