import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { OffreService } from 'src/app/services/offre.service';
import { PlaceService } from 'src/app/services/place.service';
import { RegisterService } from 'src/app/services/register.service';
import { SpecialityService } from 'src/app/services/speciality.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css'],
})
export class LayoutComponent implements OnInit {
  listuser: any;
  listplace: any;
  listspeciality: any;
  listoffre: any;
  x: any;
  submitted = false;
  loginForm: FormGroup;
  registerCandidatForm: FormGroup;
  registerEmployerForm: FormGroup;
  fileToUpload: Array<File> = [];
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(
    private registerservice: RegisterService,
    private formBuilder: FormBuilder,
    private placeservice: PlaceService,
    private loginservice: LoginService,
    private specialityservice: SpecialityService,
    private route: Router,
    private offreservice: OffreService
  ) {}

  ngOnInit(): void {
    this.registerCandidatForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      place: ['', Validators.required],
      specialites: ['', Validators.required],
      description: ['', Validators.required],
      role: [''],
    });

    this.registerEmployerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      website: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      description: ['', Validators.required],
      password: ['', Validators.required],
      specialites: ['', Validators.required],
      place: ['', Validators.required],
      role: [''],
    });

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.getalluser();
    this.getallplace();
    this.getallspeciality();
    this.getalloffre();
  }

  getalluser() {
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data'];
      console.log('liste user :', this.listuser);
    });
  }

  getallplace() {
    this.placeservice.getallplaces().subscribe((res: any) => {
      this.listplace = res['data'];
      console.log('liste place :', this.listplace);
    });
  }
  getallspeciality() {
    this.specialityservice.getallspeciality().subscribe((res: any) => {
      this.listspeciality = res['data'];
      console.log('liste speciality :', this.listspeciality);
    });
  }

  getalloffre() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'];
      console.log('liste offres :', this.listoffre);
    });
  }

  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  get f() {
    return this.registerCandidatForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.registerCandidatForm.value.email);
    console.log(this.registerCandidatForm.value.role);

    for (var i = 0; i < this.listuser.length; i++) {
      if (this.registerCandidatForm.value.email === this.listuser[i].email)
        this.x = 1;
    }
    //this.registerCandidatForm.patchValue({ role: 'candidat' });
    let formdata = new FormData();
    formdata.append('role', 'candidat');
    formdata.append('firstname', this.registerCandidatForm.value.firstname);
    formdata.append('lastname', this.registerCandidatForm.value.lastname);
    formdata.append('email', this.registerCandidatForm.value.email);
    formdata.append('mobile', this.registerCandidatForm.value.mobile);
    formdata.append('password', this.registerCandidatForm.value.password);
    formdata.append('description', this.registerCandidatForm.value.description);
    formdata.append('specialites', this.registerCandidatForm.value.specialites);
    formdata.append('place', this.registerCandidatForm.value.place);
    //formdata.append('pictures', this.registerCandidatForm.value.provider);
    formdata.append('image', this.fileToUpload[0]);

    this.registerservice.register(formdata).subscribe(
      (res: any) => {
        if ((res.status = 200)) {
          Swal.fire({ icon: 'success', title: 'Your Account is created' });
          this.onReset();
        }
      },
      (err) => {
        if (this.x === 1) {
          Swal.fire({ icon: 'error', title: 'Email already used' });
        } else {
          Swal.fire({ icon: 'error', title: 'Your Account is not created' });
        }
      }
    );
  }

  onSubmitEmp() {
    this.submitted = true;
    for (var i = 0; i < this.listuser.length; i++) {
      if (this.registerEmployerForm.value.email === this.listuser[i].email)
        this.x = 1;
    }
    let formdata = new FormData();
    formdata.append('role', 'entreprise');
    formdata.append('firstname', this.registerEmployerForm.value.firstname);
    formdata.append('website', this.registerEmployerForm.value.website);
    formdata.append('email', this.registerEmployerForm.value.email);
    formdata.append('mobile', this.registerEmployerForm.value.mobile);
    formdata.append('password', this.registerEmployerForm.value.password);
    formdata.append('description', this.registerEmployerForm.value.description);
    formdata.append('specialites', this.registerEmployerForm.value.specialites);
    formdata.append('place', this.registerEmployerForm.value.place);
    //formdata.append('pictures', this.registerEmployerForm.value.provider);
    formdata.append('image', this.fileToUpload[0]);

    this.registerservice.register(formdata).subscribe(
      (res: any) => {
        if ((res.status = 200)) {
          Swal.fire({ icon: 'success', title: 'Your Account is created' });
          this.onReset();
        }
      },
      (err) => {
        if (this.x === 1) {
          Swal.fire({ icon: 'error', title: 'Email already used' });
        } else {
          Swal.fire({ icon: 'error', title: 'Your Account is not created' });
        }
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.registerCandidatForm.reset();
    this.registerEmployerForm.reset();
  }

  login() {
    console.log('bonjour :', this.loginForm.value);
    this.submitted = true;
    this.loginservice.login(this.loginForm.value).subscribe(
      (res: any) => {
        console.log('Login ', res);
        if ((res.status = 200)) {
          Swal.fire({
            icon: 'success',
            title: 'user found',
            text: 'email valid',
            footer: 'password valid',
          });
          console.log('ttttttttttt : ', res.Token);
          this.route.navigateByUrl('/listoffre');
          localStorage.setItem('userconnect', JSON.stringify(res.User));
          localStorage.setItem('token', res.Token);
          localStorage.setItem('role', res.User.role);
          localStorage.setItem('state', '0');
        }
      },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'user not found',
          text: 'email invalid',
          footer: 'password invalid',
        });
      }
    );
  }

  onResetLogin() {
    this.submitted = false;
    this.loginForm.reset();
  }
}
