import { Component, OnInit } from '@angular/core';
import { PlaceService } from 'src/app/services/place.service';
import { RegisterService } from 'src/app/services/register.service';
import { SpecialityService } from 'src/app/services/speciality.service';

@Component({
  selector: 'app-listentreprise',
  templateUrl: './listentreprise.component.html',
  styleUrls: ['./listentreprise.component.css'],
})
export class ListentrepriseComponent implements OnInit {
  listuser: any;
  nbrcompanies: any;
  listoffre: any;
  listspeciality: any;
  listplaces: any;
  constructor(
    private registerservice: RegisterService,
    private specialityservice: SpecialityService,
    private placeservice: PlaceService
  ) {}

  ngOnInit(): void {
    this.getalluser();
    this.getallspeciality();
    this.getallplaces();
  }

  getalluser() {
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data'].filter(
        (element: any) => element.role == 'entreprise'
      );

      console.log('liste entreprise :', this.listuser);
      this.nbrcompanies = this.listuser.length;

      // console.log('aaaaaaaaaa: ', this.listuser[1]._id);
    });
  }

  getallspeciality() {
    this.specialityservice.getallspeciality().subscribe((res: any) => {
      this.listspeciality = res['data'];
      console.log('liste speciality :', this.listspeciality);
    });
  }

  getallplaces() {
    this.placeservice.getallplaces().subscribe((res: any) => {
      this.listplaces = res['data'];
      console.log('liste places :', this.listplaces);
    });
  }

  OnChangeSpeciality(event: any) {
    console.log('detected value speciality', event.target.value);
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data']
        .filter((el: any) => el.role == 'entreprise')
        .filter((el: any) => el.specialites.name == event.target.value);
      console.log('liste entreprise by speciality', this.listuser);
    });
  }

  OnChangePlace(event: any) {
    console.log('detected value place', event.target.value);
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data']
        .filter((el: any) => el.role == 'entreprise')
        .filter((el: any) => el.place.name == event.target.value);
      console.log('liste entreprise by place', this.listuser);
    });
  }
}
