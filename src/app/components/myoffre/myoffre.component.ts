import { Component, OnInit } from '@angular/core';
import { CandidatureService } from 'src/app/services/candidature.service';
import { OffreService } from 'src/app/services/offre.service';

@Component({
  selector: 'app-myoffre',
  templateUrl: './myoffre.component.html',
  styleUrls: ['./myoffre.component.css'],
})
export class MyoffreComponent implements OnInit {
  listoffre: any;
  list: any;
  x: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(
    private offreservice: OffreService,
    private candidatureservice: CandidatureService
  ) {}

  ngOnInit(): void {
    this.getoffre();
    this.getoneoffre();
  }

  getoffre() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.entreprise._id == this.userconnect._id
      );
      console.log('liste my offre :', this.listoffre);
    });
  }

  getoneoffre() {
    this.offreservice
      .getoneoffre('62485a77f71ffd5661532722')
      .subscribe((res: any) => {
        this.list = res['data'];
        console.log('Nbr candidature :', this.list.candidatures.length);
        this.x = this.list.candidatures.length;
      });
  }
}
