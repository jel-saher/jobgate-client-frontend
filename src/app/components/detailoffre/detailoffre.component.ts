import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CandidatureService } from 'src/app/services/candidature.service';
import { OffreService } from 'src/app/services/offre.service';
import { PlaceService } from 'src/app/services/place.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.component.html',
  styleUrls: ['./detailoffre.component.css'],
})
export class DetailoffreComponent implements OnInit {
  id = this.activateroute.snapshot.params['id'];
  byid: any;
  candidatureForm: FormGroup;
  submitted = false;

  fileToUpload: Array<File> = [];
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(
    private activateroute: ActivatedRoute,
    private offreservice: OffreService,
    private placeservice: PlaceService,
    private formBuilder: FormBuilder,
    private candidatureservice: CandidatureService
  ) {}

  ngOnInit(): void {
    this.candidatureForm = this.formBuilder.group({
      date: ['', Validators.required],
      cv: ['', Validators.required],
      candidat: ['', Validators.required],
      id_offre: ['', Validators.required],
    });
    this.getoneoffre();
    
  }

  isentreprise(){
    return this.userconnect.role=="entreprise" ?true:false;
  }

  getoneoffre() {
    this.offreservice.getoneoffre(this.id).subscribe((res: any) => {
      this.byid = res['data'];
      console.log('Detail :  ', this.byid);
    });
  }

  handleFileInput(files: any) {
    this.fileToUpload = <Array<File>>files.target.files;
    console.log(this.fileToUpload);
  }

  Addcandidature() {
    this.submitted = true;

    let formdata = new FormData();
    //formdata.append('pictures', this.registerCandidatForm.value.provider);
    formdata.append('candidat', this.userconnect._id);
    formdata.append('id_offre', this.id);
    formdata.append('cv', this.fileToUpload[0]);
    // stop here if form is invalid
    this.candidatureservice.addcandidature(formdata).subscribe((res: any) => {
      //consommer la méthode du service addclient
      console.log('response', res);
      Swal.fire('Candidature ajouter'); //pour afficher une alerte
    });

    // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }
  onReset() {
    this.submitted = false;
    this.candidatureForm.reset();
  }
}
