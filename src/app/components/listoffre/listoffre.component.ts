import { Component, OnInit } from '@angular/core';
import { OffreService } from 'src/app/services/offre.service';
import { PlaceService } from 'src/app/services/place.service';
import { SpecialityService } from 'src/app/services/speciality.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listoffre',
  templateUrl: './listoffre.component.html',
  styleUrls: ['./listoffre.component.css'],
})
export class ListoffreComponent implements OnInit {
  listoffre: any;
  listplaces: any;
  nbroffre: any;
  byid: any;
  listspeciality: any;
  constructor(
    private offreservice: OffreService,
    private placeservice: PlaceService,
    private specialityservice: SpecialityService
  ) {}

  ngOnInit(): void {
    this.getalloffre();
    this.getallplaces();
    this.getallspeciality();
  }

  getallspeciality() {
    this.specialityservice.getallspeciality().subscribe((res: any) => {
      this.listspeciality = res['data'];
    });
  }

  getallplaces() {
    this.placeservice.getallplaces().subscribe((res: any) => {
      this.listplaces = res['data'];
      console.log('liste places :', this.listplaces);
    });
  }

  getalloffre() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.confirmed == true
      );
      console.log('liste offre :', this.listoffre);

      this.nbroffre = this.listoffre.length;
    });
  }

  deleteoffre(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.offreservice.deleteoffre(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your Offre has been deleted.', 'success');
          this.getalloffre();
        });
      }
    });
  }

  OnChangePlace(event: any) {
    console.log('detected value place', event.target.value);
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.entreprise.place.name == event.target.value
      );
      console.log('list offre type :', this.listoffre);
    });
  }

  OnChangeSpeciality(event: any) {
    console.log('detected value speciality', event.target.value);
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (el: any) => el.entreprise.specialites.name == event.target.value
      );
      console.log('liste offre by speciality', this.listoffre);
    });
  }
}
