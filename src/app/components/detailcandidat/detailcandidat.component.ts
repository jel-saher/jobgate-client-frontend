import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-detailcandidat',
  templateUrl: './detailcandidat.component.html',
  styleUrls: ['./detailcandidat.component.css'],
})
export class DetailcandidatComponent implements OnInit {
  id = this.activateroute.snapshot.params['id'];
  byid: any;

  constructor(
    private activateroute: ActivatedRoute,
    private registerservice: RegisterService
  ) {}

  ngOnInit(): void {
    this.getoneuser();
  }

  getoneuser() {
    this.registerservice.getbyid(this.id).subscribe((res: any) => {
      this.byid = res['data'];
      console.log('Detail :  ', this.byid);
    });
  }
}
