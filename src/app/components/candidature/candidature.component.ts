import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConnectableObservable } from 'rxjs';
import { CandidatureService } from 'src/app/services/candidature.service';
import { OffreService } from 'src/app/services/offre.service';

@Component({
  selector: 'app-candidature',
  templateUrl: './candidature.component.html',
  styleUrls: ['./candidature.component.css'],
})
export class CandidatureComponent implements OnInit {
  id = this.activateroute.snapshot.params['id'];
  listcandidature: any;

  constructor(
    private activateroute: ActivatedRoute,
    private offreservice: OffreService,
    private candidatureservice: CandidatureService
  ) {}

  ngOnInit(): void {
    console.log('id : ', this.id);
    this.getalloffre();
  }

  getalloffre() {
    this.candidatureservice.getallcandidature().subscribe((res: any) => {
      this.listcandidature = res['data'].filter(
        (element: any) => element.id_offre._id == this.id
      );
      console.log('liste candidature :', this.listcandidature);
    });
  }
}
