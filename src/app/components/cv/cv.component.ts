import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CandidatureService } from 'src/app/services/candidature.service';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css'],
})
export class CvComponent implements OnInit {
  id = this.activateroute.snapshot.params['id'];
  candidature: any;
  pdfSrc: any;

  constructor(
    private activateroute: ActivatedRoute,
    private candidatureservice: CandidatureService
  ) {}

  ngOnInit(): void {
    this.getcandidature();
  }

  getcandidature() {
    this.candidatureservice.getcandidature(this.id).subscribe((res: any) => {
      this.candidature = res['data'];
      console.log('candidature :', this.candidature);
      this.pdfSrc = 'http://localhost:3000/getfile/' + this.candidature.cv;
    });
  }
}
