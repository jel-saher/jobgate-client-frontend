import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
})
export class MessageComponent implements OnInit {
  listMsg: any;
  id = this.activeroute.snapshot.params['id'];
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(
    private msgservice: MessageService,
    private activeroute: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.getAllMsg();
  }
  getAllMsg() {
    return this.msgservice.getallMsg().subscribe((res: any) => {
      this.listMsg = res['data'].filter(
        (element: any) => element.entreprise._id == this.id
      );
      console.log(this.listMsg);
    });
  }
  deleteMsg(id: any) {
    this.msgservice.deletemessage(id).subscribe((res: any) => {
      this.getAllMsg();
    });
  }
}
