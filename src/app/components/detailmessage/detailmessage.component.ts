import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';

@Component({
  selector: 'app-detailmessage',
  templateUrl: './detailmessage.component.html',
  styleUrls: ['./detailmessage.component.css']
})
export class DetailmessageComponent implements OnInit {

  id=this.activeroute.snapshot.params['id'];
  message: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(private msgservice:MessageService, private activeroute: ActivatedRoute) { }
  ngOnInit(): void {
    this.getMsg();
  }
getMsg(){
  return this.msgservice.getmessage(this.id).subscribe((res:any)=>{
this.message=res['data'],
console.log(this.message);
  })
}
}
