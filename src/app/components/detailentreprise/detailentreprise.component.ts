import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'src/app/services/message.service';
import { OffreService } from 'src/app/services/offre.service';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detailentreprise',
  templateUrl: './detailentreprise.component.html',
  styleUrls: ['./detailentreprise.component.css'],
})
export class DetailentrepriseComponent implements OnInit {
  listoffre: any;
  msgForm: FormGroup;
  submitted = false;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  id = this.activateroute.snapshot.params['id'];
  byid: any;

  constructor(
    private activateroute: ActivatedRoute,
    private registerservice: RegisterService,
    private offreservice: OffreService,
    private msgservice: MessageService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.msgForm = this.formBuilder.group({
      titre: ['', Validators.required],
      description: ['', Validators.required],
      entreprise: ['', Validators.required],
      candidat: ['', Validators.required],
    });
    this.getoneuser();
    this.getoffre();
  }

  getoneuser() {
    this.registerservice.getbyid(this.id).subscribe((res: any) => {
      this.byid = res['data'];
      console.log('Detail :  ', this.byid);
    });
  }

  getoffre() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.entreprise._id == this.id
      );
      console.log('liste my offre :', this.listoffre);
    });
  }

  addMsg() {
    this.submitted = true;
    this.msgForm.patchValue({
      entreprise: this.id,
      candidat: this.userconnect._id,
    });
    this.msgservice.addmessage(this.msgForm.value).subscribe((res: any) => {
      console.log('response', res);
      Swal.fire('message envoyé ');
    });
  }
  onReset() {
    this.submitted = false;
    this.msgForm.reset();
  }
}
