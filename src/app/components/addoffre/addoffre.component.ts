import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OffreService } from 'src/app/services/offre.service';
import { PlaceService } from 'src/app/services/place.service';
import { SpecialityService } from 'src/app/services/speciality.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.component.html',
  styleUrls: ['./addoffre.component.css'],
})
export class AddoffreComponent implements OnInit {
  postjobForm: FormGroup;
  submitted = false;
  categorys: any;
  listcategory: any;
  listplace: any;
  listeskils: any;
  listentreprises: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  constructor(
    private formBuilder: FormBuilder,
    private serviceoffre: OffreService,
    private categoryservice: SpecialityService,
    private serviceplace: PlaceService,
    private route :Router
  ) {}
  ngOnInit() {
    this.postjobForm = this.formBuilder.group({
      titre: ['', Validators.required],
      type: ['', Validators.required],
      description: ['', Validators.required],
      entreprise: ['', Validators.required],
    });
    this.getoneplace();
    this.getonecatgory();
  }
  // convenience getter for easy access to form fields
  addoffree() {
    this.submitted = true;
    this.postjobForm.patchValue({
      entreprise: this.userconnect._id,
    });
    // stop here if form is invalid
    this.serviceoffre.addoffre(this.postjobForm.value).subscribe((res: any) => {
      //consommer la méthode du service addclient
      console.log('response', res);
      Swal.fire('Offre ajouté ajouté'); //pour afficher une alerte
      this.route.navigateByUrl('/listoffre');
    });
    // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  }

  getoneplace() {
    this.serviceplace
      .getoneplace(this.userconnect.place)
      .subscribe((res: any) => {
        this.listplace = res['data'];
        console.log('list place', this.listplace);
      });
  }

  getonecatgory() {
    this.categoryservice
      .getbyidspeciality(this.userconnect.specialites)
      .subscribe((res: any) => {
        this.listcategory = res['data'];
        console.log('list speciality', this.listcategory);
      });
  }

  /* getallskills() {
  this.skilservice.getskills().subscribe((res: any) => {
    this.listeskils = res['data'];
    console.log('list places', this.listeskils);
  });
} */

  onReset() {
    this.submitted = false;
    this.postjobForm.reset();
  }
}
