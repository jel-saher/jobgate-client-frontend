import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddoffreComponent } from './components/addoffre/addoffre.component';
import { CandidatureComponent } from './components/candidature/candidature.component';
import { CvComponent } from './components/cv/cv.component';
import { DetailcandidatComponent } from './components/detailcandidat/detailcandidat.component';
import { DetailentrepriseComponent } from './components/detailentreprise/detailentreprise.component';
import { DetailmessageComponent } from './components/detailmessage/detailmessage.component';
import { DetailoffreComponent } from './components/detailoffre/detailoffre.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListcandidatComponent } from './components/listcandidat/listcandidat.component';
import { ListentrepriseComponent } from './components/listentreprise/listentreprise.component';
import { ListoffreComponent } from './components/listoffre/listoffre.component';
import { MessageComponent } from './components/message/message.component';
import { MycandidatureComponent } from './components/mycandidature/mycandidature.component';
import { MyoffreComponent } from './components/myoffre/myoffre.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '', component: LayoutComponent },
      { path: 'listoffre', component: ListoffreComponent },
      { path: 'detailoffre/:id', component: DetailoffreComponent },
      { path: 'listcandidat', component: ListcandidatComponent },
      { path: 'detailcandidat/:id', component: DetailcandidatComponent },
      { path: 'listeentreprise', component: ListentrepriseComponent },
      { path: 'addoffre', component: AddoffreComponent },
      { path: 'myoffre', component: MyoffreComponent },
      { path: 'candidature/:id', component: CandidatureComponent },
      { path: 'cv/:id', component: CvComponent },
      { path: 'mycandidature', component: MycandidatureComponent },
      { path: 'detailentreprise/:id', component: DetailentrepriseComponent },
      { path: 'listmessage/:id', component: MessageComponent },
      { path: 'detailmessage/:id', component: DetailmessageComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
